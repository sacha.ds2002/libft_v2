/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putptr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/10 14:40:34 by sada-sil          #+#    #+#             */
/*   Updated: 2023/08/29 15:29:54 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"

int	ft_putptr(unsigned long long ptr)
{
	int		len;

	len = 0;
	len += ft_putstr("0x");
	len += ft_puthex(ptr, 'x');
	return (len);
}
