/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puthex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/22 14:13:12 by sada-sil          #+#    #+#             */
/*   Updated: 2023/08/29 15:29:44 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"

static int	ft_numlen(unsigned int n)
{
	int	i;

	i = 1;
	while (n >= 16)
	{
		n /= 16;
		i++;
	}	
	return (i);
}

static void	ft_putnbr_base(unsigned long long nb, char *base)
{
	if (nb >= 16)
		ft_putnbr_base((nb / 16), base);
	ft_putchar(base[nb % 16]);
}

int	ft_puthex(int n, char x)
{
	char			*base;
	unsigned int	c;

	if (x == 'X')
		base = "0123456789ABCDEF";
	else
		base = "0123456789abcdef";
	c = (unsigned int)n;
	ft_putnbr_base(c, base);
	return (ft_numlen(c));
}
