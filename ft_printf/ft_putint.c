/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putint.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/10 13:56:13 by sada-sil          #+#    #+#             */
/*   Updated: 2023/08/29 15:29:47 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"

static int	ft_intlen(int n)
{
	int	i;

	i = 1;
	if (n < 0)
	{
		if (n == -2147483648)
			return (11);
		n *= -1;
		i++;
	}
	while (n >= 10)
	{
		n /= 10;
		i++;
	}	
	return (i);
}

int	ft_putint(int n)
{
	int	len;

	len = ft_intlen(n);
	ft_putnbr_fd(n, 1);
	return (len);
}
