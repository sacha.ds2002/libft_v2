/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/12 10:20:21 by sada-sil          #+#    #+#             */
/*   Updated: 2023/08/29 14:09:39 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

void	*ft_calloc(size_t count, size_t size)
{
	void	*v;
	char	*str;
	size_t	i;

	i = 0;
	v = (void *)malloc(count * size);
	if (!v)
		return (0);
	str = (char *)v;
	while (i < count * size)
		str[i++] = 0;
	return (v);
}

size_t	ft_strlen(const char *str)
{
	size_t	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

void	ft_strlcpy(char *dst, const char *src, size_t dstsize)
{
	size_t	i;

	i = 0;
	if (dstsize)
	{
		while (i < dstsize - 1 && src[i])
		{
			dst[i] = src[i];
			i++;
		}
		dst[i] = 0;
	}
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*str;
	int		len;
	int		i;
	int		j;

	i = -1;
	j = -1;
	len = ft_strlen(s1) + ft_strlen(s2);
	str = (char *)ft_calloc(sizeof(char), (len + 1));
	if (!str)
		return (0);
	while (s1[++i])
		str[i] = s1[i];
	while (s2[++j])
		str[i++] = s2[j];
	str[i] = '\0';
	free((char *)s1);
	return (str);
}

char	*ft_strndup(const char *s1, int size)
{
	char	*str;
	int		i;

	i = 0;
	str = (char *)ft_calloc((size + 1), sizeof(char));
	if (!str)
	{
		free(str);
		return (0);
	}
	ft_strlcpy(str, s1, size + 1);
	if (size + 1)
	{
		while (i < size + 1 - 1 && s1[i])
		{
			str[i] = s1[i];
			i++;
		}
		str[i] = 0;
	}
	return (str);
}
