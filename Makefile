# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/10/24 11:08:32 by sada-sil          #+#    #+#              #
#    Updated: 2023/08/29 15:38:44 by sada-sil         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libft.a

CC = cc
CFLAGS = -Wall -Werror -Wextra
RM = rm -f

SRCS_LIBFT = 	$(addprefix libft/, \
					ft_strlen.c \
					ft_isalpha.c \
					ft_isalnum.c \
					ft_isdigit.c \
					ft_isascii.c \
					ft_isprint.c \
					ft_bzero.c \
					ft_memset.c \
					ft_memcpy.c \
					ft_memmove.c \
					ft_strlcpy.c \
					ft_strlcat.c \
					ft_toupper.c \
					ft_tolower.c \
					ft_strchr.c \
					ft_strrchr.c \
					ft_strncmp.c \
					ft_memchr.c \
					ft_memcmp.c \
					ft_strnstr.c \
					ft_atoi.c \
					ft_calloc.c \
					ft_strdup.c \
					ft_strndup.c \
					ft_substr.c \
					ft_strjoin.c \
					ft_strtrim.c \
					ft_split.c \
					ft_itoa.c \
					ft_strmapi.c \
					ft_striteri.c \
					ft_putchar_fd.c \
					ft_putstr_fd.c \
					ft_putendl_fd.c \
					ft_putnbr_fd.c \
					ft_lstnew.c \
					ft_lstadd_front.c \
					ft_lstsize.c \
					ft_lstlast.c \
					ft_lstadd_back.c \
					ft_lstdelone.c \
					ft_lstclear.c \
					ft_lstiter.c \
					ft_lstmap.c \
					gnl_strjoin.c )

SRCS_GNL = 	$(addprefix gnl/, \
				get_next_line.c \
				get_next_line_utils.c )

SRCS_FT_PRINTF = $(addprefix ft_printf/, \
					ft_printf.c \
					ft_putchar.c \
					ft_putstr.c \
					ft_putint.c \
					ft_putuint.c \
					ft_putnbr_base.c\
					ft_putnbr_unsigned_fd.c )

OBJS_GNL = ${SRCS_GNL:.c=.o}
OBJS_LIBFT = ${SRCS_LIBFT:.c=.o}
OBJS_FT_PRINTF = ${SRCS_FT_PRINTF:.c=.o}

${NAME}:	${OBJS_LIBFT} ${OBJS_GNL} ${OBJS_FT_PRINTF}
	ar rc ${NAME} ${OBJS_LIBFT} ${OBJS_GNL} ${OBJS_FT_PRINTF}
	ranlib ${NAME}
	ranlib ${NAME}

all:	${NAME}

clean:
	${RM} ${OBJS_LIBFT} ${OBJS_GNL} ${OBJS_FT_PRINTF}

fclean: clean
	${RM} ${NAME}

re: clean all

.PHONY: all clean fclean re
