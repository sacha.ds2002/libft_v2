/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/28 16:29:23 by sada-sil          #+#    #+#             */
/*   Updated: 2023/01/24 15:04:59 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_calloc(size_t count, size_t size)
{
	void	*v;
	char	*str;
	size_t	i;

	i = 0;
	v = (void *)malloc(count * size);
	if (!v)
		return (0);
	str = (char *)v;
	while (i < count * size)
		str[i++] = 0;
	return (v);
}
